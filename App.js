import * as React from 'react';
import {View, Text, Touchable, TouchableOpacity} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Screen1 from './src/screens/Screen1';
import Screen2 from './src/screens/Screen2';

function HomeScreen() {
  const nav = {
    backgroundColor: 'red',
    padding: 10,
    width: 400,
    textAlign: 'center',
    color: 'white',
    fontSize: 22,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5,
  };
  const navigation = useNavigation();

  function goToFirst() {
    navigation.navigate('screen1');
  }
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={nav}>Home Screen</Text>
      <TouchableOpacity onPress={goToFirst}>
        <Text style={nav}>Go Screen1</Text>
      </TouchableOpacity>
    </View>
  );
}
const Stack = createNativeStackNavigator();

function App() {
  const data = {
    text: 'hello world',
  };
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="screen1" component={Screen1} />
        <Stack.Screen name="screen2" component={Screen2} />

        {/* <Stack.Screen name="screen2">
          {props => <Screen2 {...props} data={data} />}
        </Stack.Screen> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
