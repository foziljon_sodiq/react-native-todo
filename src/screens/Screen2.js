import {View, Text} from 'react-native';
import React from 'react';

export default function Screen2({route}) {
  console.log('budata', route.params.text);
  let text = route.params.productItem;

  return (
    <View>
      <Text style={{fontSize: 22, textAlign: 'center'}}>{text}</Text>
    </View>
  );
}
