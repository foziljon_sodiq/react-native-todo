import * as React from 'react';
import {
  Button,
  View,
  Text,
  TextInput,
  StyleSheet,
  KeyboardAvoidingView,
} from 'react-native';
import {useState} from 'react';

function Screen1({navigation}) {
  const [text, setText] = useState('');
  const [todo, setTodo] = useState([]);

  const addText = () => {
    let a = new Date();
    if (text) {
      setTodo([...todo, {text, id: a.getTime()}]);
      setText('');
    }
  };

  const deleteText = id => {
    let filter = todo.filter(item => item.id !== id);
    setTodo(filter);
  };

  const goToItem = item => {
    navigation.navigate('screen2', {productItem: item});
  };

  return (
    <View>
      <View style={{padding: 10}}>
        <KeyboardAvoidingView style={styles.wrap}>
          <TextInput
            style={styles.input}
            placeholder="Type new task"
            onChangeText={newText => setText(newText)}
            defaultValue={text}
          />
          <Text style={styles.btn} onPress={addText}>
            Add
          </Text>
          <Text style={styles.btn} onPress={() => navigation.goBack()}>
            Back
          </Text>
        </KeyboardAvoidingView>

        <View>
          {todo.map(text => (
            <View key={text.id} style={styles.items}>
              <Text onPress={() => goToItem(text.text)} style={styles.item}>
                {text.text}
              </Text>
              <Text onPress={() => deleteText(text.id)}>X</Text>
            </View>
          ))}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  wrap: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  input: {
    padding: 10,
    backgroundColor: '#ccc',
    height: 40,
    width: 250,
  },
  btn: {
    backgroundColor: 'blue',
    color: 'white',
    width: 50,
    lineHeight: 40,
    borderRadius: 3,
    textAlign: 'center',
  },
  items: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '90%',
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 30,
    marginTop: 5,
  },
  item: {
    fontSize: 22,
    fontWeight: 'bold',
  },
});
export default Screen1;

// ... other code from the previous section
